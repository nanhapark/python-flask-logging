from flask import Flask
app = Flask(__name__)

import logging, logging.config, yaml
logging.config.dictConfig(yaml.load(open('logging.conf')))

@app.route("/")
def hello():
  app.logger.warning('A warning occurred (%d apples)', 42)
  app.logger.error('An error occurred')
  app.logger.info('Info')
  return "Hello World!"

if __name__ == "__main__":
  app.debug = False
  app.run()
